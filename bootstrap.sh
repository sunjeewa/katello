#!/bin/bash
# Thu  8 Jun 2017 10:14:07 AEST
# sanjeewa.wijesundara@datacom.com.au
#
# Red Hat Satellite 6 install on EC2 RHEL 7 

# Apply environment file
source ../.ENV

OS_VERSION="7"

#========== No changes required below this line ===============================
logger -t system-setup "Starting RH Satellite installation"
logger -t system-setup "Waiting 60 seconds for system startup to complete"
sleep 60
logger -t system-setup "Initiating RH Satellite setup"

# FQDN and Public IP 
PUBLIC_HOSTNAME=`curl http://169.254.169.254/latest/meta-data/public-hostname`
PUBLIC_IP=`curl http://169.254.169.254/latest/meta-data/public-ipv4`


# Setup firewall Rules. 
echo "Setting up firewall rules"
yum install -y firewalld
systemctl enable firewalld
systemctl start firewalld
firewall-cmd --permanent \
--add-port="22/tcp" \
--add-port="443/tcp" \
--add-port="5671/tcp" \
--add-port="80/tcp" \
--add-port="8140/tcp" \
--add-port="9090/tcp" \
--add-port="8080/tcp"
firewall-cmd --complete-reload

# Add public fqdn to /etc/hosts
echo "${PUBLIC_IP} ${PUBLIC_HOSTNAME}" >> /etc/hosts
hostname "${PUBLIC_HOSTNAME}"

# Register instance with Red Hat subscription manager
logger -t system-setup "Trying to register system with Red Hat Subscription Manager"
subscription-manager register --force --username=${RH_USER} --password=${RH_PASSWORD}

# Get Pool ID
sleep 5
POOL_ID=`subscription-manager list --available --all | grep "Pool ID" | awk '{print $3}'`

logger -t system-setup "Received pool id $POOL_ID"

# Subscribe with pool ID
subscription-manager subscribe --pool=${POOL_ID}
subscription-manager repos --disable "*"
subscription-manager repos --enable rhel-${OS_VERSION}-server-satellite-6.0-rpms
subscription-manager repos --enable rhel-${OS_VERSION}-server-rpms
subscription-manager repos --enable rhel-server-rhscl-${OS_VERSION}-rpms

logger -t system-setup "Enabled repos"

# Install NTPD
yum install -y ntp
systemctl enable ntpd
systemctl start ntpd

# Install sos
yum install -y sos

# Install katello
yum install -y katello

# Install vim
yum install -y vim

logger -t system-setup "About to run katello-installer "
# Configure Katello
#katello-installer -v -d --foreman-admin-password=${KATELLO_ADMIN_PASSWD}
